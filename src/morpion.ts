let playerText = document.getElementById('playerText');
let restartBTn = document.getElementById('restartBtn');
let boxes = Array.from(document.getElementsByClassName('box'));
let board = document.getElementById("gameboard");
let fin = document.getElementById("fin");

console.log(boxes);

const O_TEXT = 'O';
const X_TEXT = 'X';
const winningCombos=[
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
]

let currentPlayer = X_TEXT;
let spaces = Array(9).fill(null);


const startGame = () =>{
    boxes.forEach(box => box.addEventListener('click', boxClicked))
}
startGame()


restartBTn.addEventListener('click', restart);







function restart(){
    spaces.fill(null);
    boxes.forEach(box =>{
        box.innerHTML =''
    })
    playerText.textContent = 'Morpion';
    currentPlayer = X_TEXT;
}

function playerHasWon(){
    for (const item of winningCombos) {
        let [a, b, c] = item;
        if(spaces[a] && spaces[a] == spaces[b] && spaces[a] == spaces[c]){
            return [a, b, c];
            

        
        }

    }
    return false

}


function boxClicked(e:any){
    const id = e.target.id
    
    if(!spaces[id]){
        spaces[id] = currentPlayer;
        e.target.innerText = currentPlayer;

        if(playerHasWon() !== false){
            playerText.textContent=`${currentPlayer} has won !`; 


        }
        
        currentPlayer = currentPlayer == X_TEXT ? O_TEXT:X_TEXT;
    }

}